
const {generateMetric} = require('./common/metrics');

module.exports = {
  recordAccountMetric: generateMetric(
    'Usage',
    {
      MetricName: 'Active Accounts',
      Dimensions: [{'Name': 'Accounts', 'Value': 'Charged'}]
    }, 
    (val, currentTimeUTC, executionTime ) => `Charged ${val} accounts for time slot: ${currentTimeUTC} in ${executionTime} seconds.`
  )
}
  