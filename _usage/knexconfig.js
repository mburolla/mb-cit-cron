
const knexConfig = {
  client: 'mysql2',
  connection: {
    host : process.env.DB_HOST,
    user : process.env.DB_USER,
    password: process.env.DB_PWD,
    database : process.env.DB_DATABASE
  },
  pool: { min: 0}
}

module.exports = require('knex')(knexConfig)
