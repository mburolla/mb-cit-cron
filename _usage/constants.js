
module.exports = {
  ACCOUNT_STATUS: {
    ACTIVE: 1,
    DELINQUENT: 3
  },

  CASE_STATUS: {
    OPEN: 1,
    CLOSING: 2,
    CLOSED: 3
  }
}