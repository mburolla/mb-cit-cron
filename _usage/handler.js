
const {getBillableAccounts, completeCases } = require('./proxyDB');
const {chargeAccount} = require('./proxyStripe');
const {recordAccountMetric} = require('./proxyMetric');

module.exports.recordCaseUsage = async () => {
  await recordAccountMetric(async () => {
    const chargedAccounts = await getBillableAccounts()
      .map(async account => {
        await chargeAccount(account, account.case_count);
      })
      .catch(x => console.info('error in case processing', x))
    
    return chargedAccounts.length;
  }); 

};


module.exports.closeCases = async () => {
  return await completeCases();
}


