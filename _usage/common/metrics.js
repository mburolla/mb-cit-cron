const moment = require('moment-timezone');
const proxySES = require('./proxySES');

const AWS = require('aws-sdk');

AWS.config.update({region: 'us-east-1'});
const cloudwatch = new AWS.CloudWatch({apiVersion: '2015-10-07'});

const putMetricData = async function(name, metric, value) {
  const params = {
    MetricData: [{
      ...metric,
      'Unit': 'Count',
      'Value': value
    }],
    Namespace: `${process.env.CIT_ENV}/${name}`
  };
  return retval = await cloudwatch.putMetricData(params).promise();
};

module.exports.generateMetric = (name, metric, template) => async (fnc) => { 
  const startTime = Date.now();
  const currentTimeUTC = quantize(moment());

  try {
    const value = await fnc();
    const executionTime = Math.round((Date.now() - startTime)/1000);
    
    await putMetricData(name, metric, value || 0);
    await putMetricData(name, {
      'MetricName': 'Execution Time',
      'Dimensions': [{'Name': 'Time', 'Value': 'Seconds'}]
    }, executionTime);
    const message = template(value, currentTimeUTC, executionTime);
    const subject = `CRON: ${name} Job - Summary Report (${process.env.CIT_ENV})`;
    await proxySES.sendEmail(process.env.EMAIL_SUMMARY_REPORT, message, subject);
    console.log(`*** CRON ${name} Job has finished, total run time: ${executionTime} seconds. ***` );
  
  } catch (err) {
    await reportFatalError(err, name, currentTimeUTC);
  }
  return;
}


/**
 * Alert the authorities.
 */
async function reportFatalError(err, name, currentTimeUTC) {
  const message = `Time slot: ${currentTimeUTC} ERROR: ${err}`;
  const subject = `CRON: ${name} Job - FATAL ERROR in (${process.env.CIT_ENV})`;
  await proxySES.sendEmail(process.env.EMAIL_SUMMARY_REPORT, message, subject );
  console.log(`*** FATAL ERROR: ${err} ***`);
}

/**
 * Rounds a datetime up or down to the nearest half hour. 
 *   
 * Round up:
 *     2019-02-07 23:50:00 ==> 2019-02-08 00:00:00
 *     2019-02-08 00:20:00 ==> 2019-02-08 00:30:00
 *
 * Round down:
 *     2019-02-08 00:02:00 ==> 2019-02-08 00:00:00
 *     2019-02-08 00:40:00 ==> 2019-02-08 00:30:00
 */
function quantize(dateTime) {
  let remainder = 0;
  if ((dateTime.minute() >= 1 && dateTime.minute() <= 15) || 
    (dateTime.minute() >= 30 && dateTime.minute() <= 45)) {
    // Round down.
    remainder = (dateTime.minute() % 30) * -1;
  } else if (dateTime.minute() != 0 && dateTime.minute() != 30 ) {
    // Round up.
    remainder = 30 - (dateTime.minute() % 30);
  }
  return moment(dateTime).add(remainder, "minutes").format("YYYY-MM-DD HH:mm:00");
}
