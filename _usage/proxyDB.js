const db = require('./knexconfig');
const {CASE_STATUS} = require('./constants')

module.exports.getBillableAccounts = () => 
  db
    .select('account.account_id', 'stripe_customer_id')
    .count('court_case.court_case_id as case_count')
    .from('account')
    .innerJoin('court_case', 'account.account_id', 'court_case.account_id')
    .innerJoin('case_status', 'court_case.case_status_id', 'case_status.case_status_id')
    .innerJoin('account_status', 'account.account_status_id', 'account_status.account_status_id')
    .where('account_status.is_billable', true)
    .where('case_status.is_billable', true)
    .groupBy('account.account_id')
    .orderBy('account.account_id')


module.exports.completeCases = () =>
  db('court_case')
    .where({'court_case.case_status_id': CASE_STATUS.CLOSING})
    .update({'court_case.case_status_id': CASE_STATUS.CLOSED})
