
const stripeConfig = require('../stripefile')[process.env.NODE_ENV || 'dev'];
const stripe = require("stripe")(stripeConfig.secretKey);
const knex = require('knex')(require('../knexfile')[process.env.NODE_ENV || 'dev'])

module.exports.createTestData = async function(caseCount=2) {

  //stripe
  const customerData = await stripe.customers.create({
    description: 'Customer for Test',
    source: "tok_mastercard"
  })

  const customerId = customerData.id;

  const subscription = await stripe.subscriptions.create({
    customer: customerId,
    items: [{plan: stripeConfig.planKey}]
  });

  //knex - db
  const account = {
    account_status_id: 1, 
    name: 'Billing Test account',
    stripe_customer_id: customerData.id
  };

  const accountIds = await knex.batchInsert('account', [account])
    .returning('account_id')

  const user = {
    name: 'test user',
    account_id: accountIds[0]
  }
  const userIds = await knex.batchInsert('user', [user])
    .returning('user_id')

  const courtCase = {
    account_id: accountIds[0],
    user_id: userIds[0],
    court_id: 1,
    sub_category_id: 1,
    case_status_id: 1, 
    name: 'Billing Test'
  }

  for (let i=0; i < caseCount; i++) {
    console.info('case created', i);
    await knex.batchInsert('case', [courtCase])
      .catch(x => console.info('case creation failure', x));
  }

  knex.destroy();
};

