
const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);


//TODO: make sure subscription matches stripeConfig.planKey
function findSubscription(subscriptions, customerId){
    if (subscriptions.length !== 1) {
        console.info('Account/Customer has 0 or multiple subscrptions', customerId, subscriptions.length);
    }

    return subscriptions[0]; //most recent is on front of list
}

module.exports.chargeAccount = async function chargeAccount(account, count){

  await stripe.customers.retrieve(account.stripe_customer_id)
    .then(async customer => {
        const subscription = findSubscription(customer.subscriptions.data, account.stripe_customer_id);
        if (!subscription || (subscription.items.data.length === 0)) {
            console.warn('Account/Customer has no subscriptions or latest subscription has no subscrption items', account.stripe_customer_id)
            return ; //can't process subscription
        }
        if (!subscription || subscription.items.data.length > 1) {
            console.warn('Account/Customer has multiple subscription items for latest subscription', account.stripe_customer_id);
        }

        const si = subscription.items.data[0].id; // get most recent subscription item
        const currentTime = Math.floor((new Date()).getTime()/1000);

        const metered = await stripe.usageRecords.create(si, {
            quantity: count,
            timestamp: currentTime,
            action: "increment"
        });
    })
    .then(() => console.info('successful usage report for ', account.account_id))
    .catch((e) => console.info('failed usage report for', account.account_id, e));
}
