# CalendarIt CRON Repo
Contains the following Serverless CRON jobs:
- Reminder
- Usage
- Utility

#### Reminder
Runs every 30 minutes:
- Gets all the deadlines (emails) that need to be sent for the current day (reminder_email table)
- Sends emails at the user's specified time slot
- Sends summary and failure emails, logs metric data to CloudWatch
- [More Info](/reminder/README.md)

#### Usage
Runs once every 8 hours:
- On the first of every month, move cases in CLOSING to CLOSED
- Sends the number of billable cases to Stripe
- [More Info](/usage/README.md)

#### Utility
Runs once every day at 8:00 UTC and optionally performs database tasks to keep things running smooth.

# Getting Started
- Clone this repository
- cd to {reminder|usage|utility}
- Each directory is a separate [Serverless](https://serverless.com/) project with its own NPM package
- Execute: `npm install`

# Deployment
- Individual deployment
  - cd to {reminder|usage|utilty}
  - Staging: `sls deploy`
  - Production: `sls deploy --stage prod`
- Mass deploy:  
  - `./deploy_prod.sh`|`./deploy_stage.sh`

# AWS CRON Expressions
- AWS CRON expressions are different and have 6 parts: [AWS CRON](https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html#CronExpressions)
- Every 5th minute:  `*/5 * * * ? *`
- Every 30th minute: `*/30 * * * ? *`
- Every day at 8:00 UTC: `0 8 * * ? *`
- Every day at 22:00 UTC: `0 22 * * ? *`

# MISC
- `/_boiler plate` is a directory that can be used to create new cron jobs
- Built using:
  * NPM v6.4.1
  * Microsoft Visual Code v1.30.2
  * Node v8.1