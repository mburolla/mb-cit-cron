//
// Name: proxyAuroraDB.js
// Auth: Martin Burolla
// Date: 04/22/2019
// Desc: The one and only interface into AWS Aurora DB for the Utility CRON job.
//

const CN_DeleteToken_Utility = 'CN_DeleteToken_Utility';
const CN_TruncateError_Utility = 'CN_TruncateError_Utility';
const CN_OptimizeTables_Utility = 'CN_OptimizeTables_Utility';
const CN_DeletePasswordHash_Utility =  'CN_DeletePasswordHash_Utility';
const CN_DeleteRuleInstaniation_Utility = 'CN_DeleteRuleInstaniation_Utility';

module.exports.deleteUnusedTokens = async (pool) => {
  const sql = `call ${CN_DeleteToken_Utility}();`; 
  const [[[retval]]] = await pool.query(sql);
  return retval;
};

module.exports.truncateErrorTable = async (pool) => {
  const sql = `call ${CN_TruncateError_Utility}();`;
  const [[[retval]]] = await pool.query(sql);
  return retval;
};

module.exports.optimizeTables = async (pool) => {
  const sql = `call ${CN_OptimizeTables_Utility}();`;
  const [[[retval]]] = await pool.query(sql);
  return retval;
};

module.exports.cleanupRuleInstaniationTable = async (pool) => {
  const sql = `call ${CN_DeleteRuleInstaniation_Utility}();`;
  const [[[retval]]] = await pool.query(sql);
  return retval;
};

module.exports.deleteOldPasswordHashes = async (pool) => {
  const sql = `call ${CN_DeletePasswordHash_Utility}();`;
  const [[[retval]]] = await pool.query(sql);
  return retval;
};
