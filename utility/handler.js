// 
// Name: handler.js
// Auth: Martin Burolla
// Date: 4/22/2019
// Desc: The one and only handler for the Utility CRON job.
//
//       Performs misc tasks to keep things running smooth.
//

const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});
const rds = new AWS.RDS({apiVersion: '2014-10-31'});

const _ = require('lodash');
const dbpools = require('./dbpools');
const proxySES = require('./proxySES');
const proxyAuroraDB = require('./proxyAuroraDB');

const promisePool = dbpools.createRWPool().promise();

/**
 * Main run loop for the Utility job.
 */
module.exports.utility = async () => { 
  console.log(`*** CRON Utility Job is running in: ${process.env.CIT_ENV} ***`);
  try {
    const startTime = Date.now();
    
    // await stopDBClusters(); // yml => cron(0 8 * * ? *)
   
    // Uncomment these lines:
    const r1 = await truncateErrorTable(promisePool);
    const r2 = await optimizeTables(promisePool);
    const r3 = await cleanupRuleInstaniationTable(promisePool);
    const r4 = await deleteOldPasswordHashes(promisePool);
    const r5 = await deleteUnusedTokens(promisePool);
    await sendSummaryEmail(startTime, r1, r2, r3, r4, r5);
  }
  catch(err) {
    await sendFatalErrorReport(err);
  }
};

const stopDBClusters = async () => {
  try {
    await rds.stopDBCluster({ DBClusterIdentifier: process.env.DB_CLUSTER_ID}).promise();
    console.log("Stopping cluster.");
  } catch(err) {
    console.log(err.code, err.statusCode, err.message, `DB Cluster: ${process.env.DB_CLUSTER_ID} is already stopped.`);
  }
}

/**
 * Delete old rows that hold errors returned from the API.
 */
const truncateErrorTable = async (promisePool) => {
  console.log(`*** Truncating error table ***`);
  const retval = await proxyAuroraDB.truncateErrorTable(promisePool);
  return retval;
}

/**
 * Indexes mission critical tables.
 */
const optimizeTables = async (promisePool) => {
  console.log(`*** Optimizing tables ***`);
  const retval = await proxyAuroraDB.optimizeTables(promisePool);
  return retval;
}

/**
 * Deletes old rows from the rule_instaniation table.
 */
const cleanupRuleInstaniationTable = async (promisePool) => {
  console.log(`*** Archiving rule instaniation table ***`);
  const retval = proxyAuroraDB.cleanupRuleInstaniationTable(promisePool);
  return retval;
}

/**
 * Deletes previous password hashes.
 */
const deleteOldPasswordHashes = async (promisePool) => {
  console.log(`*** Deleting old password hashes ***`);
  const retval = await proxyAuroraDB.deleteOldPasswordHashes(promisePool);
  return retval;
}

/**
 * Deletes old unused tokens related to confirming accounts,
 * refreshing the access token and password reset tokens.
 */
const deleteUnusedTokens = async (promisePool) => {
  console.log(`*** Deleting unused tokens ***`);
  const retval = await proxyAuroraDB.deleteUnusedTokens(promisePool);
  return retval;
}

/**
 * Send email of results of this job.
 */
const sendSummaryEmail = async (startTime, r1, r2, r3, r4, r5) => {
  console.log(`*** Sending summary email to: ${process.env.EMAIL_SUMMARY_REPORT} ***`);
  const summary = { 
    errorTable: r1.error_row_count, 
    optimizeTables: r2.message,
    ruleInstaniationArchive: r3.num_rows,
    numOldPwdHashes: r4.num_rows_deleted,
    numRefreshTokens: r5.num_token_refresh,
    numConfirmationTokens: r5.num_token_confirmation,
    numPwdResetTokens: r5.num_token_password_reset,
    executionTime: (Date.now() - startTime)/1000,
  };

  const htmlMessage = buildHTMLMessage(summary);
  await proxySES.sendEmail(process.env.EMAIL_SUMMARY_REPORT, htmlMessage, `CRON: Utility Job - Summary Report (${process.env.CIT_ENV})`);
  console.log(`*** CRON Utility Job has finished. ***`);
}

/**
 * Alert the authorities.
 */
const sendFatalErrorReport = async (error) => {
  console.log(`*** FATAL ERROR CRON Utility Job: ${error} stack: ${error.stack} in ${process.env.CIT_ENV} ***`);
  const htmlMessage = buildHTMLFatalErrorMessage(error);
  await proxySES.sendEmail(process.env.EMAIL_FAILURE_REPORT, htmlMessage, `FATAL ERROR: CRON Utility (${process.env.CIT_ENV})`);
}

/**
 * HTML TEMPLATES
 */
function buildHTMLMessage(summary) {
  let compiled = _.template(`
   <html>
     <body>
       <h2>Utility Job Summary Report</h2>
       <hr>
       <table border=0>
          <tr>
            <td>Error Table Row Count</td>
            <td><%= errorTableRows %></td>
          </tr>
          <tr>
            <td>Optimized Tables</td>
            <td><%= optimized %></td>
          </tr>
          <tr>
            <td>Rule Instaniation Cleanup Row Count</td>
            <td><%= ruleInstaniationArchive %></td>
          </tr>
          <tr>
            <td>Old Confirmation Tokens</td>
            <td><%= confirmationTokens %></td>
          </tr>
            <tr>
            <td>Old Password Reset Tokens</td>
          <td><%= pwdResetTokens %></td>
          </tr>
          <tr>
            <td>Old Refresh Tokens</td>
            <td><%= refreshTokens %></td>
          </tr>
          <tr>
            <td>Old Password Hashes Deleted</td>
            <td><%= pwdHashes %></td>
          </tr>
          <tr>
            <td>Execution Time (sec)</td>
            <td><%= executionTime %></td>
          </tr>
       </table>
       <p>&nbsp;</p>
       <img src="https://s3.amazonaws.com/cit-media/cit-logo.png" width="200"/>
     </body>
  </html>`);
  return htmlMessage = compiled({ 
      errorTableRows: summary.errorTable, 
      optimized: summary.optimizeTables,
      ruleInstaniationArchive: summary.ruleInstaniationArchive,
      pwdHashes: summary.numOldPwdHashes,
      executionTime: summary.executionTime,
      confirmationTokens: summary.numConfirmationTokens,
      pwdResetTokens: summary.numPwdResetTokens,
      refreshTokens: summary.numRefreshTokens
    });
}

function buildHTMLFatalErrorMessage(error) {
  let compiled = _.template(`
   <html>
     <body>
       <h2>Utility Job Fatal Error</h2>
       <table border=1>
         <tr>
           <td>Error Message</td>
           <td><%= errorMessage %></td>
         </tr>
         <tr>
           <td>Stack Trace</td>
           <td><%= stackTrace %></td>
        </tr>
       </table>
     </body>
  </html>`);
  return htmlMessage = compiled({ 
      errorMessage: error,
      stackTrace: error.stack
    });
}
