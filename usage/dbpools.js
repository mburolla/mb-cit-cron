//
// Name: dbPools.js
// Auth: Martin Burolla
// Date: 02/06/2019
// Desc: A module that stores all the database pools.
//

const mysql = require('mysql2');

module.exports.createRWPool = function() {
  const pool = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PWD,
    database: process.env.DB_DATABASE,
    queueLimit: process.env.DB_POOLS_QUEUE_LIMIT,
    connectionLimit: process.env.DB_POOLS_CONNECTION_LIMIT,
    waitForConnections: true
  });
  return pool;
};
