//
// Name: handler.js
// Auth: Martin Burolla
// Date: 02/23/2019
// Desc: The one and only handler for the Usage CRON job.
//
//       Queries the database for the number of billable cases for each
//       CalendarIt account, converts these records to Stripe usage 
//       records and sends them to Stripe.
//

const _ = require('lodash');
const moment = require('moment-timezone');
const dbpools = require('./dbpools');
const proxySES = require('./proxySES');
const proxyStripe = require('./proxyStripe');
const proxyAuroraDB = require('./proxyAuroraDB');
const proxyCloudWatch = require ('./proxyCloudWatch');

const pool = dbpools.createRWPool();

/**
 * The main run loop.
 */
module.exports.usage = async () => { 
  const startTime = Date.now();
  const currentTimeUTC = quantize(moment());
  try {
    const numCasesClosed = await closeCasesFirstDayOfMonth(pool);
    const billableCases = await getBillableCasesForAccounts(pool);
    const results = await sendUsageRecordsToStripe(billableCases);
    await sendSummaryReportAndMetrics(results, numCasesClosed, startTime, currentTimeUTC);
  } 
  catch (err) {
    await sendFatalErrorReport(err);
  }
};

/**
 * Close any cases that are in the CLOSING state if it's the first day of the month.
 */
const closeCasesFirstDayOfMonth = async (pool) => {
  console.log(`*** CRON Usage Job has started in: ${process.env.CIT_ENV}. ***`);
  return await proxyAuroraDB.closeCasesFirstDayOfMonth(pool);;
}

/**
 * Get the number of billable cases for each CalendarIt account.
 */
const getBillableCasesForAccounts = async (pool) => {
  console.log(`*** Getting billable cases from database ***`);
  return await proxyAuroraDB.getBillableCasesForAccounts(pool);
}

/**
 * Convert the billableCases into usage records and send them to Stripe.
 * Action: 'set' overrides any existing usage record quantities for this timestamp.
 */
const sendUsageRecordsToStripe = async (billableCases) => {
  console.log(`*** Preparing to send ${billableCases.length} usage records ***`);
  let promises = [];
  billableCases.forEach(item => {
    const usageRecord = {
      quantity: item.num_billable_cases,
      timestamp: item.timestamp_utc,
      action: 'set'
    };
    promises.push(proxyStripe.uploadUsageRecordForCustomer(usageRecord, item.stripe_customer_id));
  });
  console.log(`*** Sent ${billableCases.length} usage records to Stripe ***`);
  return await Promise.all(promises);
}

/**
 * Send an email report and push success/fail data to CloudWatch graphs in AWS.
 */
const sendSummaryReportAndMetrics = async (results, numCasesClosed, startTime, currentTimeUTC) => {
  const resultsOK = results.filter(item => item.error === '');
  const resultsError = results.filter(item => item.error !== '');
  const executionTime = Math.round((Date.now() - startTime)/1000);

  const usageSuccessTable = buildHtmlTable(resultsOK);
  const usageFailedTable = buildHtmlTable(resultsError);
  const htmlMessage = buildHTMLSummaryMessage(results, currentTimeUTC, resultsOK, resultsError, numCasesClosed.cases, executionTime, usageSuccessTable, usageFailedTable);

  await proxyCloudWatch.putMetricDataExecutionTime(process.env.CIT_ENV, executionTime);
  await proxyCloudWatch.putMetricDataUsageRecords(process.env.CIT_ENV, resultsOK.length);
  await proxyCloudWatch.putMetricDataErrorUsageRecords(process.env.CIT_ENV, resultsError.length);
  await proxySES.sendEmail(process.env.EMAIL_SUMMARY_REPORT, htmlMessage, `CRON: Usage Job - Summary Report (${process.env.CIT_ENV})`);
  
  console.log(`*** Attempted to send ${results.length} usage records for time slot ${currentTimeUTC} with ${resultsError.length} errors in ${executionTime} seconds ***`);
  console.log(`*** CRON Usage Job has finished in: ${executionTime} seconds ***`);
}

/**
 * Alert the authorities.
 */
const sendFatalErrorReport = async (error) => {
  console.log(`*** FATAL ERROR CRON Usage Job: ${error} stack: ${error.stack} in ${process.env.CIT_ENV} ***`);
  const htmlMessage = buildHTMLFatalErrorMessage(error);
  await proxySES.sendEmail(process.env.EMAIL_FAILURE_REPORT, htmlMessage, `FATAL ERROR: CRON Usage (${process.env.CIT_ENV})`);
}

/**
 * Rounds a datetime up or down to the nearest hour or half-hour mark.
 */
function quantize(dateTime) {
  let remainder = 0;
  if (dateTime.minute() >= 45 && dateTime.minute() <= 59) {
    remainder = 30 - (dateTime.minute() % 30);
  } else if (dateTime.minute() >= 1 && dateTime.minute() <= 15) {
    remainder = (dateTime.minute() % 30) * -1;
  } else if (dateTime.minute() >= 16 && dateTime.minute() <= 29) {
    remainder = 30 - (dateTime.minute() % 30);
  } else if (dateTime.minute() >= 31 && dateTime.minute() <= 44) {
    remainder = (dateTime.minute() % 30) * -1;
  }
  return moment(dateTime).add(remainder, "minutes").format("YYYY-MM-DD HH:mm:00");
}

/**
 * Creates a nice HTML for the usage data.
 */
function buildHtmlTable(usageRows) {
  let retval = '<table border=1>';
  retval += `<tr>
      <td><b>Stripe Customer ID</b></td>
      <td><b>Subscription Item ID</b></td>
      <td><b>Quanity</b></td>
      <td><b>Timestamp</b></td>
    </tr>`;
    
  _.forEach(usageRows, item => {
    retval += `<tr>
      <td>${item.stripeCustomerId}</td>
      <td>${item.subscriptionItemId}</td>
      <td>${item.quantity}</td>
      <td>${item.timestamp}</td>
    </tr>`
  });
  
  retval += '</table>';
  return retval;
}

/**
 * HTML TEMPLATES
 */
function buildHTMLSummaryMessage(results, currentTimeUTC, passed, failed, numCasesClosed, executionTime, usageSuccessTable, usageFailedTable) {
  let compiled = _.template(`
   <html>
     <body>
       <h2>Usage Job Summary Report</h2>
       <hr>
       <p>Attempted to send <%= numRecords %> usage records to Stripe for quantized timeslot: <%= currentTimeUTC %> UTC.</p>
       <h4>Results</h4>
       <table border=1>
         <tr>
           <td>Usage records sent:</td>
           <td><%= numPassed %></td>
         </tr>
         <tr>
           <td>Usage records failed:</td>
           <td><%= numFailed %></td>
         </tr>
         <tr>
           <td>Cases closed:</td>
           <td><%= numCasesClosed %></td>
         </tr>
         <tr>
           <td>Execution time (sec):</td>
           <td><%= executionTime %></td>
         </tr>
       </table>

       <h4>Failures</h4>
       <%= usageFailedTable %>

       <h4>Success</h4>
       <%= usageSuccessTable %>
       <p>&nbsp;</p>
       <img src="https://s3.amazonaws.com/cit-media/cit-logo.png" width="200"/>
     </body>
  </html>`);
  return htmlMessage = compiled(
    { numRecords: results.length, 
      currentTimeUTC,
      numPassed: passed.length, 
      numFailed: failed.length, 
      numCasesClosed, 
      executionTime,
      usageSuccessTable,
      usageFailedTable
    });
}

function buildHTMLFatalErrorMessage(error) {
  let compiled = _.template(`
   <html>
     <body>
       <h2>Usage Job Fatal Error</h2>
       <table border=1>
         <tr>
           <td>Error Message</td>
           <td><%= errorMessage %></td>
         </tr>
         <tr>
           <td>Stack Trace</td>
           <td><%= stackTrace %></td>
        </tr>
       </table>
     </body>
  </html>`);
  return htmlMessage = compiled({ 
      errorMessage: error,
      stackTrace: error.stack
    });
}