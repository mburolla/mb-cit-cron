# Usage CRON Job
CalendarIt accounts are billed for the number of cases that are in the OPEN or CLOSING state.  Every day the Usage CRON job sends the number of billable cases for each account to Stripe.  On the first of the month, Stripe invoices every customer.  At the beginning of the month, the Usage CRON job updates cases from CLOSING to CLOSED so that customers do not get billed for these cases for the next monthly billing cycle starting on the first of the month.  

# Approach
A Serverless CRON job at a specified interval and executes two stored procs. One stored proc updates case state on the first of the month, the second stored proc returns the number of billable cases for each CalendarIt account.  The Stripe customer Id is stored in the Accounts table and is used to query the Stripe subscription item Id for the customer.  A usage record is sent to Stripe using the Stripe subscription item id and the number of billable cases (quantity).  Summary and failure emails are sent at the end of the job. 

Note: This approach is fine for the MVP but a better solution is to queue usage records to [SQS](https://serverless.com/framework/docs/providers/aws/events/sqs/) and haved them trigger a Lambda function which sends them to Stripe.

# Architecture
The Usage CRON job uses the proxy pattern as found in the API, where a proxy represents a boundry to an external services such as Stripe, CloudWatch, SES, etc.  Proxies are used help provide a separation between business logic and data.  Proxies do not contain any business logic and solely focus on providing a clean wrapper around a resource, shuffling data to/from the business logic.

Promises Async/Await are used to handle async code.

# Initial KPIs
TBD

# Links
- [Staging Log Files](https://console.aws.amazon.com/cloudwatch/home?region=us-east-1#logStream:group=/aws/lambda/stage-cron-case-usage)
- [Production Log Files](https://console.aws.amazon.com/cloudwatch/home?region=us-east-1#logStream:group=/aws/lambda/prod-cron-case-usage)
- [CloudWatch Graphs](https://console.aws.amazon.com/cloudwatch/home?region=us-east-1#dashboards:name=CRON)

# Logs
The CloudWatch logs have the following format:

<img src=https://gitlab.com/EL-CIT/cit-cron/uploads/1ec4b4cd442dd13bcc1bc10c39f2fe6b/image.png></img>

# MySQL Troubleshooting
```
call CN_UpdateCase_Closed();
call CN_UpdateSelectCases_UsageCron();
select * from case_status;
select * from account_status;
```

# Stripe Notes
- When a customer is created for the first time a $0.00 invoice is created
- Client side tokenization is used to "wipe our hands clean" from doing any credit card processing
- The `billing_cycle_anchor` is used when the subscription is created and specifies the day of the month (as a timestamp) to bill the customer
- Once a plan is created it cannot be edited:

> "Once a plan has been created, only the metadata and nickname can be modified; the amount, 
> currency, and interval are fixed. Should you need to change any of these parameters, a new 
> plan must be created instead." 

- Modify this link to view all the usage records for a subscription item id:

```
https://dashboard.stripe.com/test/subscription_items/<YOUR SUBSCRIPTION ITEM ID HERE>/usage_records
```

# Billing Reconciliation
There's currently no reconciliation process in place, however [this](https://stripe.com/docs/api/usage_records/subscription_item_summary_list) Stripe API might come in handy in the future.  Stripe has the ability to create [scheduled queries](https://stripe.com/docs/api/sigma/scheduled_queries) which can be used to reconcile the cases actually billed in Stripe and the cases that we should have billed in our database (they should be the same).

# Invoice Calculation
- Stripe creates monthly invoices using a tiered volume pricing structure.  The number of billable cases is aggregated for each account at the database and sent to Stripe.  Stripe is configured to bill the customer according to their last usage record in their billing cycle.  Customers are billed based on where their number of billable cases falls in this pricing tier:

<img src=https://gitlab.com/EL-CIT/cit-cron/uploads/93321109e006ed0e075bcb617b39909e/image.png></img>

# Account Provisioning
The account provisioning API endpoint (`/account/provision`) creates a Stripe customer and creates a subcription for new customers signing up for the service for the first time.  The plan id (e.g. plan_E4PzHJVlIx8uNS) stored in the API config file is passed into the create subscription call, along with the billing cycle anchor (a timestamp for the first day of the next month).  The call to create subscription also creates a unique subscription item for the subscription.  The subscription item is used for associating usage records for this customer.

# Stripe Model
The CalendarIt Stripe billing model is very simple, consisting of a 1:1 relationship for the following entities:
- [Product](https://stripe.com/docs/api/service_products) (e.g. prod_E4Pw6Kc3OX9mgE)
- [Plan](https://stripe.com/docs/api/plans) (e.g. plan_E4PzHJVlIx8uNS)
- [Customer](https://stripe.com/docs/api/customers) (e.g. cus_EZzxHS3M2g4SZ8)
- [Subscription](https://stripe.com/docs/api/subscriptions) (e.g. sub_EZzx6UE0CWIdwG)
- [Subscription_items](https://stripe.com/docs/api/subscription_items) (e.g. si_EZzxvCOKLCyDck)
