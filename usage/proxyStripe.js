//
// Name: proxyStripe.js
// Auth: Martin Burolla
// Date: 2/23/2019
// Desc: The one and only interface for Stripe.
// Calls: stripe.customers.retrieve
//        stripe.usageRecords.create
//
// https://dashboard.stripe.com/test/subscription_items/si_EZzxvCOKLCyDck/usage_records
//

const stripe = require("stripe")(process.env.STRIPE_KEY);

/**
 * Upload a usage record to Stripe for this customer, handle any errors.
 */
module.exports.uploadUsageRecordForCustomer = async function (usageRecord, stripeCustomerId) {
  return new Promise(async (resolve) => {
    let retval = {
      stripeCustomerId: stripeCustomerId,
      subscriptionItemId: 0,
      quantity: usageRecord.quantity,
      timestamp: usageRecord.timestamp,
      error: ''
    };

    try {
      // Recall this customer's unique subscription item id.
      const customer = await stripe.customers.retrieve(stripeCustomerId);
      retval.subscriptionItemId = customer.subscriptions.data[0].items.data[0].id

      // Send the subscription item id and usage record to Stripe.
      const stripeResponse = await stripe.usageRecords.create(retval.subscriptionItemId, usageRecord);

      // Compare our usage record with the response from Stripe.
      if (usageRecord.quantity === stripeResponse.quantity &&
        usageRecord.timestamp === stripeResponse.timestamp && 
        retval.subscriptionItemId === stripeResponse.subscription_item) { 
        resolve(retval); 
      } else {
        // Highly unlikely we will get here.
        retval.error = `Usage record mismatch at: ${usageRecord.timestamp} for: ${stripeCustomerId}.`; 
        resolve(retval);
      }
    } catch (err) {
      retval.error = err.message;
      resolve(retval);
    }  
  });
};
