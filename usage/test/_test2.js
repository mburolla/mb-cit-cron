
const _ = require('lodash');

const resultsOK = [{"stripeCustomerId":"cus_EfTLTgYEHkLmfM","subscriptionItemId":"si_EfTLsxAf94xDxl","quantity":2,"timestamp":1556206217,"error":""},{"stripeCustomerId":"cus_EfTMpE64azIIrG","subscriptionItemId":"si_EfTMYQsFbhlZrF","quantity":19,"timestamp":1556206217,"error":""},{"stripeCustomerId":"cus_EfTSe5U0mQszrB","subscriptionItemId":"si_EfTSQPSgczyIIO","quantity":3,"timestamp":1556206217,"error":""}];
 
let htmlTable = '<table border=1>';
htmlTable += `<tr>
    <td>Stripe Customer ID</td>
    <td>Subscription Item ID</td>
    <td>Quanity</td>
    <td>Timestamp</td>
  </tr>`;
  
_.forEach(resultsOK, item => {
  htmlTable += `<tr>
    <td>${item.stripeCustomerId}</td>
    <td>${item.subscriptionItemId}</td>
    <td>${item.quantity}</td>
    <td>${item.timestamp}</td>
  </tr>`
});

htmlTable += '</table>';

console.log(htmlTable);