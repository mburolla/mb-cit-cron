//
// Name: _test.js
// Auth: Martin Burolla
// Date: 2/23/2019
// Desc: The test driver for the usage cron.

const mysql = require('mysql2');
const dbpools = require('./dbpools');
const proxySES = require('./proxySES');
const proxyStripe = require('./proxyStripe');
const proxyAuroraDB = require('./proxyAuroraDB');
const proxyCloudWatch = require ('./proxyCloudWatch');

const pool = mysql.createPool({
  host: 'stage-cit-db.cbmuxjob1c3x.us-east-1.rds.amazonaws.com',
  user: 'sa',
  password: 'Lr2kG69pE4NzZFGy',
  database: 'calendaritdb',
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0
});

/**
 * This doesn't scale well.  A better approach is to queue the billableCases into SQS 
 * and create a Lambda SQS trigger to send the usage records to Stipe.  This is fine
 * for the MVP.
 */
async function foo() {
  try {
    const [[[numCasesClosed]]] = await proxyAuroraDB.closeCasesFirstDayOfMonth(pool);
    const [[billableCases]] = await proxyAuroraDB.getBillableCasesForAccounts(pool);
    let promises = [];
    billableCases.forEach(item => {
      promises.push(proxyStripe.createUsageRecordForCustomer(item.stripe_customer_id, item.num_billable_cases));
    });
    const results = await Promise.all(promises);  // TODO: Dump this to an S3 Bucket.
    await proxySES.sendEmail('mburolla@gmail.com', `Sent ${results.length} usage records. <br> Closed ${numCasesClosed.cases} cases. <br><p>Usage Record Responses<p> ${JSON.stringify(results)}`, 'CRON: Usage Job - Summary Report (STAGING)');
    //await proxyCloudWatch.putMetricDataUsageRecords('STAGING', results.length);
    //await proxyCloudWatch.putMetricDataExecutionTime('STAGING', 1);
  } catch (err) {
    console.log(`*** ${err} ***`);
    await proxySES.sendEmail('mburolla@gmail.com', `${err}`, 'FATAL ERROR: CRON Usage');
  }
  console.log('done');
  return;
}

foo();


