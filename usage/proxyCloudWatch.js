//
// Name: proxyCloudWatch.js
// Auth: Martin Burolla
// Date: 2/09/2019
// Desc: The one and only interface into AWS CloudWatch.
//

const AWS = require('aws-sdk');

AWS.config.update({region: 'us-east-1'});
const cloudwatch = new AWS.CloudWatch({apiVersion: '2015-10-07'});

module.exports.putMetricDataUsageRecords = async function(envName, numRecords) {
  const params = {
    MetricData: [{
      'MetricName': 'Usage Records',
      'Dimensions': [{'Name': 'Number Usage Records', 'Value': 'Sent'}],
      'Unit': 'Count',
      'Value': numRecords
    }],
    Namespace: envName 
  };
  return await cloudwatch.putMetricData(params).promise();
};

module.exports.putMetricDataErrorUsageRecords = async function(envName, numRecords) {
  const params = {
    MetricData: [{
      'MetricName': 'Usage Records Errors',
      'Dimensions': [{'Name': 'Number Usage Record Errors', 'Value': 'Sent'}],
      'Unit': 'Count',
      'Value': numRecords
    }],
    Namespace: envName 
  };
  return await cloudwatch.putMetricData(params).promise();
};

module.exports.putMetricDataExecutionTime = async function(envName, numSeconds) {
  const params = {
    MetricData: [{
      'MetricName': 'Usage Execution Time',
      'Dimensions': [{'Name': 'Usage Record Time', 'Value': 'Seconds'}],
      'Unit': 'Count',
      'Value': numSeconds
    }],
    Namespace: envName 
  };
  return await cloudwatch.putMetricData(params).promise();
};
