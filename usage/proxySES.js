//
// Name: proxySES.js
// Auth: Martin Burolla
// Date: 1/31/2019
// Desc: The one and only interface into AWS SES.
//

const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});

module.exports.sendEmail = async function(address, message, subject) {
  const params = {
    Destination: { 
      ToAddresses: [address]
    },
    Message: {
      Body: { 
        Html: {
          Charset: "UTF-8",
          Data: `<html><head></head><body>${message}</body></html>`
        },
        Text: {
          Charset: "UTF-8",
          Data: message
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: subject
      }
    },
    Source: process.env.EMAIL_SOURCE, 
    ReplyToAddresses: [process.env.EMAIL_REPLY_TO],
  };       
  return await new AWS.SES({apiVersion: '2010-12-01'}).sendEmail(params).promise();
};
