//
// Name: proxyAuroraDB.js
// Auth: Martin Burolla
// Date: 02/23/2019
// Desc: The one and only interface for AuroraDB
//

const CN_UPDATECASE_CLOSED = 'CN_UpdateCase_Closed';
const CN_UPDATESELECTCASES_USAGECRON = 'CN_SelectCourtCase_UsageCron';

/**
 * Changes the status of cases that are in the CLOSING state
 * to CLOSED if today is the first day of the month.
 */
module.exports.closeCasesFirstDayOfMonth = async (pool) => {
  const sql = `call ${CN_UPDATECASE_CLOSED}();`;
  const [[[retval]]] = await pool.promise().query(sql);
  return retval;
};

/**
 * Returns the number of cases that are considered billable for every 
 * active account.
 */
module.exports.getBillableCasesForAccounts = async (pool) => {
  const sql = `call ${CN_UPDATESELECTCASES_USAGECRON}();`;
  const [[retval]] = await pool.promise().query(sql);
  return retval;
};
