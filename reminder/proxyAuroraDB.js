//
// Name: proxyAuroraDB.js
// Auth: Martin Burolla
// Date: 02/06/2019
// Desc: The one and only interface for AWS AuroraDB.
//

const CN_SELECTREMINDEREMAIL_FORTIME = 'CN_SelectReminderEmail_ForTime';
const CN_UPDATEUSER_ADVANCEREMINDERTIMES = 'CN_UpdateUser_AdvanceNextReminderTimes';
const CN_INSERTREMINDEREMAIL_NEWREMINDERSFORTDOAY = 'CN_InsertReminderEmail_NewRemindersForToday';

module.exports.getReminderEmailsForToday = async (pool) => {
  const sql = `call ${CN_INSERTREMINDEREMAIL_NEWREMINDERSFORTDOAY}();`;
  const [[[retval]]] = await pool.query(sql);
  return retval;
};

module.exports.updateNextReminderTimes = async (pool) => {
  const sql = `call ${CN_UPDATEUSER_ADVANCEREMINDERTIMES}();`;
  const [[[retval]]] = await pool.query(sql); 
  return retval;
};

module.exports.getReminderEmailsToSendForThisTime = async (pool, currentTimeUTC) => {
  const sql = `call ${CN_SELECTREMINDEREMAIL_FORTIME}('${currentTimeUTC}');`;
  const [[retval]] = await pool.query(sql);
  return retval;
};
