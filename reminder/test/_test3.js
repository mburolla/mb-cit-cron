
const proxyCloudWatch = require('./proxyCloudWatch');

async function foo() {
  const r = await proxyCloudWatch.putMetricDataReminderEmails('STAGING', 53);
  const r2 = await proxyCloudWatch.putMetricDataExecutionTime('STAGING', 5);
  console.log(r, r2);
}

foo();
