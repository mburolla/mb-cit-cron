
const messagesPerSecond = 2;
const SLEEP_TIME_MS = 1000;

async function foo() {

  let reminderEmails = [];
  for (i=0; i < 21; i++) {
    reminderEmails.push(i);
  }

  while (reminderEmails.length > 0) {
    let emailsToSend = reminderEmails.slice(0, messagesPerSecond);
    reminderEmails = reminderEmails.slice(messagesPerSecond, reminderEmails.length);

    let promises = [];
    emailsToSend.forEach( item => {
      promises.push(item);
    });

    await Promise.all(promises);
    await sleep(SLEEP_TIME_MS);
    
    console.log(emailsToSend);
    console.log(reminderEmails);
  }
}
  
const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

foo();
