const mysql = require('mysql2');
const dbpools = require('./dbpools');
const proxySES = require('./proxySES');
const moment = require('moment-timezone');
const proxyAuroraDB = require('./proxyAuroraDB');

const pool = mysql.createPool({
  host: 'stage-cit-db.cbmuxjob1c3x.us-east-1.rds.amazonaws.com',
  user: 'sa',
  password: 'Lr2kG69pE4NzZFGy',
  database: 'calendaritdb',
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0
});

async function foo() {
  const [[rows]] = await proxyAuroraDB.getEmailsToSend(pool, '2019-02-06 22:24:45');
  if (rows.length > 0) {
    console.log(`*** Preparing to send ${rows.length} emails... ***`);
    var promiseArray = [];
    rows.forEach(async row => {
      promiseArray.push(proxySES.sendEmail(row.email, row.deadline_desc, `CalendarIt Reminder for Case: ${row.case_name}`));
    });
    await Promise.all(promiseArray);
    return 'done';
  } else {
    console.log(`*** No emails sent ***`);
    return 'done';
  }
}
try {
  foo();
} catch(err) {
  console.log(err);
}

