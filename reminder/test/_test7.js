// Wraps the the deadlines into one email for each user.
// This allows multiple deadlines to be contained in one email
// for the user.

const _ = require('lodash');

let reminderEmails = [
  {email: 'mburolla@gmail.com', case_name: 'Sammy vs Dave', deadline_date_tz: '4/25/2019', deadline_desc: 'Deadline to dismiss'},
  {email: 'mburolla@gmail.com', case_name: 'Joe vs Don', deadline_date_tz: '4/25/2019', deadline_desc: 'Present to Judge'},
  {email: 'mburolla@gmail.com', case_name: 'Peter vs Paul', deadline_date_tz: '4/25/2019', deadline_desc: 'Service for appeals'},
  {email: 'joe@gmail.com', case_name: 'Rocket vs Queen', deadline_date_tz: '4/25/2019', deadline_desc: 'title4'},
  {email: 'joe@gmail.com', case_name: 'Trump vs Congress', deadline_date_tz: '4/25/2019', deadline_desc: 'title5'},
  {email: 'peter@gmail.com', case_name: 'Me vs. You', deadline_date_tz: '4/25/2019', deadline_desc: 'title6'},
];

// Transform data...
let emailsToSend = [];
_.forOwn(_.groupBy(reminderEmails, 'email'), (value, key) => {
  deadlineArray = [];
  _.forEach(value, item => {
    deadlineArray.push({
      deadDesc: item.deadline_desc, 
      dateTz: item.deadline_date_tz,
      caseName: item.case_name
    });
  })
  emailsToSend.push({ email: key, deadlines: deadlineArray});
})

// Present data...
// Convert the deadlines array to an HTML table.
_.forEach(emailsToSend, user => {
  let htmlTable = '<table>';
  _.forEach(user.deadlines, deadlines => {
    htmlTable += `<tr><td>${deadlines.deadDesc} is due on: ${deadlines.dateTz} for case: ${deadlines.caseName}.</td></tr>`
  });
  htmlTable += '</table>';
  user.deadlinesTable = htmlTable;
})

console.log(emailsToSend);
