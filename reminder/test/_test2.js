const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});

// Create CloudWatch service object
// var cloudwatchevents = new AWS.CloudWatchEvents({apiVersion: '2015-10-07'});

// var params = {
//   Entries: 
//     [
//       {
//         "Time": "2016-01-14T01:02:03Z",
//         "Source": "com.mycompany.myapp",
//         "Resources": [
//           "resource1",
//           "resource2"
//         ],
//         "DetailType": "myDetailType",
//         "Detail": "{ \"key1\": \"value1\", \"key2\": \"value2\" }"
//       }
//     ]
// };
// cloudwatchevents.putEvents(params, function(err, data) {
//   if (err) console.log(err, err.stack); // an error occurred
//   else     console.log(data);           // successful response
// });

var cloudwatchlogs = new AWS.CloudWatchLogs();

async function foo() {
  var params = {
    logEvents: [
      {
        message: 'This is a test', 
        timestamp: 0
      }
    ],
    logGroupName: '/aws/marty/log',
    logStreamName: 'streamit'
  };

  // cloudwatchlogs.putLogEvents(params, function(err, data) {
  //   if (err) console.log(err, err.stack); // an error occurred
  //   else     console.log(data);           // successful response
  // });

  try {
    const r = await cloudwatchlogs.putLogEvents(params).promise(); 
    console.log(r);
  } catch(err) {
    console.log(err);
  }
  return;
}

foo();

