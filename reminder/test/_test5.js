
const moment = require('moment');

/**
 * Rounds a datetime up or down to the nearest hour or half-hour mark.
 * I'm ugly but I work.
 */
function quantize(dateTime) {
  let remainder = 0;
  if (dateTime.minute() >= 45 && dateTime.minute() <= 59) {
    remainder = 30 - (dateTime.minute() % 30);
  } else if (dateTime.minute() >= 1 && dateTime.minute() <= 15) {
    remainder = (dateTime.minute() % 30) * -1;
  } else if (dateTime.minute() >= 16 && dateTime.minute() <= 29) {
    remainder = 30 - (dateTime.minute() % 30);
  } else if (dateTime.minute() >= 31 && dateTime.minute() <= 44) {
    remainder = (dateTime.minute() % 30) * -1;
  }
  return moment(dateTime).add(remainder, "minutes").format("YYYY-MM-DD HH:mm:00");
}

console.log(quantize(moment('2019-02-07 23:50:00'))); // Really early.
console.log(quantize(moment('2019-02-07 23:58:00'))); // A little early.
console.log(quantize(moment('2019-02-08 00:00:01'))); // Almost right on.
console.log(quantize(moment('2019-02-08 00:02:00'))); // A little late.
console.log(quantize(moment('2019-02-08 00:10:00'))); // A lot late.
console.log('-------------------');
console.log(quantize(moment('2019-02-08 00:20:00'))); // Really early.
console.log(quantize(moment('2019-02-08 00:28:00'))); // A little early.
console.log(quantize(moment('2019-02-08 00:30:01'))); // Almost right on.
console.log(quantize(moment('2019-02-08 00:32:00'))); // A little late.
console.log(quantize(moment('2019-02-08 00:40:00'))); // A lot late.
console.log('-------------------');
console.log(quantize(moment()));
console.log(quantize(moment('2019-02-10T04:00:47.004Z')));
console.log(quantize(moment('2019-02-10 04:00:00')));
