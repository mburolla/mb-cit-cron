# Reminder CRON Job
There are two key concepts in this solution that allow emails to be sent during the daylight savings time shift:

  * All times are stored as UTC in the database
  * The next run time is determined everyday for all users

# Approach
At the beginning of the next day, this job truncates and loads the `reminder_email` table with all the emails that need to be sent for the day.  The `reminder_email.reminder_time_utc` column contains the UTC time in which an email needs to be sent.  

Once the `reminder_email` table has been loaded with all the emails for the day, the  `user.next_runtime_utc` column is updated for the next day for all users.  To advance the time, this UTC time value is converted to the user's local timezone value (`user.time_zone_name`), advanced by one day, converted back to UTC and saved to the database (`user.next_runtime_utc`).  This approach ensures that users get there emails when the time changes.

Every 30 minutes the job examines the `reminder_email.reminder_time_utc` column and compares it to the current UTC time which was quantized (rounded) to the nearest hour and half-hour. If the times are roughly the same (+/- 2 minutes), an email is sent to the user.  This solution sends summary and fatal error emails, and sends custom metric data to CloudWatch graphs.

Bogus emails (e.g. dfjdfljfkdjejr@gmail.com) do not throw exceptions.

# SES Message Throttling
Currently, AWS throttles us-east-1 to 14 email messages a second.  NOTE: This limit applies to an ENTIRE AWS REGION, which means this limitation applies to both STAGING and PRODUCTION environments!  The [config files](./config/) limit the rate of emails that can be sent for each environment (10+1=11 messages/second).  This means we have 3 messages/second bandwidth remaining for the API and other services (if any).

# Architecture
The Reminder CRON job uses the proxy pattern as seen in the CIT-API repo.  Proxies act as boundry classes or service wrappers that wrap AWS services (e.g. `proxySES`, `proxyAuroraDB`).  Async/Await is used for async calls and stored procs are used to access the database.

# Initial KPIs
Load testing was performed on the Staging and Production enviroments at the same time.  A maximum of 512 messages was sent for one time slot from both environments without error.  These CloudWatch graphs indicate that the throttling is working correctly.

#### Rate: 10 Messages/Sec

<img src="https://gitlab.com/EL-CIT/cit-cron/uploads/4a5f2611c0f29611749409faf8150e78/CRON_Reminder_Prod.png">

#### Rate: 1 Message/Sec

<img src="https://gitlab.com/EL-CIT/cit-cron/uploads/32a4b3589a555ffefa56f9a4b3e85a11/CRON_Reminder_Stage.png">

No database leaks we observed in the past 6 hours in production.

<img src="https://gitlab.com/EL-CIT/cit-cron/uploads/08e7564d3401a08b46b97f3ec67424d8/CRON_Reminder_DB_Connections.png">

# Links
- [Staging Log Files](https://console.aws.amazon.com/cloudwatch/home?-region=us-east-1#logStream:group=/aws/lambda/stage-cron-reminder;streamFilter=typeLogStreamPrefix)
- [Production Log Files](https://console.aws.amazon.com/cloudwatch/home?-region=us-east-1&region=us-east-1#logStream:group=/aws/lambda/prod-cron-reminder)
- [CloudWatch Graphs](https://console.aws.amazon.com/cloudwatch/home?region=us-east-1#dashboards:name=CRON)
  
# Logs
The CloudWatch logs have the following format:

<img src=https://gitlab.com/EL-CIT/cit-cron/uploads/2a46fbbc35f5a62e8bd033f18652206d/CRON_REMINDER_LOGS.png>

# MySQL Troubleshooting

```
call CN_UpdateUser_AdvanceNextReminderTimes();
call CN_InsertReminderEmail_NewRemindersForToday();
call CN_SelectReminderEmail_ForTime('2019-02-08 21:00:00'); 

-- Deadline source table.
select deadline_desc, deadline_date_utc 
from deadline 
where user_id = 1 order by deadline_date_utc desc;

-- Time Settings.
select 
reminder_time_utc, next_run_time_utc, time_zone_name, reminder_days 
from user where user_id = 1;

-- Email queue table.
select * from reminder_email where user_id = 1;

-- Test Lamba function.
update reminder_email set reminder_time_utc = now() where user_id = 1;

-- Load testing...
insert reminder_email  
  (user_id, court_case_id, deadline_id, email, case_name, deadline_desc, deadline_date_utc, reminder_time_utc)
  Select user_id, court_case_id, deadline_id, email, case_name, deadline_desc, deadline_date_utc, reminder_time_utc  from reminder_email;

update reminder_email set reminder_time_utc = '2019-02-10 15:00:00';
```

# Send a Test Reminder Email to Yourself
Make sure the last date in the values clause is on the half hour UTC time, then wait...
```
insert reminder_email  
  (user_id, court_case_id, deadline_id, email, case_name, deadline_desc, deadline_date_utc, reminder_time_utc)
values
  (2,1,1,'mburolla@gmail.com', 'Sammy vs Dave', 'Deadline to dismiss case.', '2019-04-28 00:00:00', '2019-04-25 13:30:00');  
```
And make sure your emails are queued up...
```
call CN_SelectReminderEmail_ForTime('2019-04-25 13:30:00');
```