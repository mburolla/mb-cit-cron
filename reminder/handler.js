// 
// Name: handler.js
// Auth: Martin Burolla
// Date: 2/6/2019
// Desc: The one and only handler for the Reminder CRON job.
// 
//       Sends reminder emails for deadlines that are approaching 
//       according to a user's preference.
//

const _ = require('lodash');
const moment = require('moment-timezone');
const dbpools = require('./dbpools');
const proxySES = require('./proxySES');
const proxyAuroraDB = require('./proxyAuroraDB');
const proxyCloudWatch = require('./proxyCloudWatch');

const pool = dbpools.createRWPool().promise();

const SLEEP_TIME_MS = 1000;
const MESSAGES_PER_SECOND = process.env.EMAIL_MESSAGES_PER_SECOND; // Do not change this.

/**
 * Allows us to pause slightly to stay within our AWS SES throttling limit
 * across all environments in a region.
 */
const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

/**
 * The main run loop.
 */
module.exports.reminder = async () => { 
  // Do some work here.
};

/**
 * Obtain all the deadline reminder emails that we need to send TODAY
 * and insert them into the reminder_email table.
 * Emails only get queued once at the beginning of the day.
 */
const queueReminderEmailsForToday = async () =>  {
  console.log(`*** CRON Reminder Job has started in: ${process.env.CIT_ENV}. ***`);
  const newEmails = await proxyAuroraDB.getReminderEmailsForToday(pool);
  console.log(`*** Number of emails queued for today: ${newEmails.new_reminder_emails}. ***` );
}

/**
 * Advance the next reminder time (user.next_run_time_utc) for all users to 
 * tomorrow and adjust for the daylight savings shift if necessary.
 * Reminder times only get updated once and advance to the next day.
 */
const advanceReminderTimes = async () => {
  const updatedRows = await proxyAuroraDB.updateNextReminderTimes(pool);
  console.log(`*** Updated ${updatedRows.num_rows} user.next_run_time_utc rows. ***`);
}

/**
 * Obtain the deadline reminder emails that we need to send for this time slot. 
 */
const getReminderEmailsForTimeSlot = async (currentTimeUTC) => {
  const retval = await proxyAuroraDB.getReminderEmailsToSendForThisTime(pool, currentTimeUTC);
  console.log(`*** Sending ${retval.length} emails for this time slot: ${currentTimeUTC}. ***` );
  return retval;
}

/**
 * Group the deadlines for each user and create an HTML table for all the deadlines
 * for each user.  One email for each user may contain one to many deadlines.
 */
function formatReminderEmails(rows) {
  let retval = [];

  // Transform the data...
  _.forOwn(_.groupBy(rows, 'email'), (value, key) => {
    deadlineArray = [];
    _.forEach(value, item => {
      deadlineArray.push({
        deadDesc: item.deadline_desc, 
        dateTz: item.deadline_date_tz,
        caseName: item.case_name
      });
    })
    retval.push({ email: key, deadlines: deadlineArray});
  })

  // Present data... convert the deadlines array to an HTML table.
  _.forEach(retval, user => {
    let htmlTable = '<table>';
    _.forEach(user.deadlines, deadlines => {
      htmlTable += `<tr><td>${deadlines.deadDesc} is due on: ${deadlines.dateTz} for case: ${deadlines.caseName}.</td></tr>`
    });
    htmlTable += '</table>';
    user.deadlinesTable = htmlTable;
  })
  console.log(`*** Consolidated ${rows.length} deadlines into ${retval.length} emails. ***` );
  return retval;
}

/**
 * NOTE: This does not scale well, but this is probably good enough
 * for the MVP.  This has successfully sent 512 emails for one time slot.
 * 
 * A better approach is to queue these messages in SQS and have 
 * an SQS-Lambda function pull from the queue and send emails.
 */
const sendReminderEmails = async (formattedEmails, currentTimeUTC) => {
  console.log(`*** Preparing to send ${formattedEmails.length} emails for this time slot: ${currentTimeUTC}. ***`);
  let batchNumber = 0;
  const numEmails = formattedEmails.length;
  
  while (formattedEmails.length > 0) {
    let emailsToSend = formattedEmails.slice(0, MESSAGES_PER_SECOND);
    formattedEmails = formattedEmails.slice(MESSAGES_PER_SECOND, formattedEmails.length);

    let promises = [];
    emailsToSend.forEach(item => {
      const htmlMessage = buildHTMLMessage(item.deadlinesTable);
      promises.push(proxySES.sendEmail(item.email, htmlMessage, 'CalendarIt Reminder'));
    });

    await Promise.all(promises);
    await sleep(SLEEP_TIME_MS);
    batchNumber++;
    console.log(`*** Sent group: ${batchNumber} of ${Math.round(numEmails/MESSAGES_PER_SECOND)}. ***`);
  }
}

/**
 * Finish.
 */
const sendSummaryReportAndMetrics = async (startTime, currentTimeUTC, formattedEmails) => {
  const executionTime = Math.round((Date.now() - startTime)/1000);
  await proxyCloudWatch.putMetricDataReminderEmails(process.env.CIT_ENV, formattedEmails.length || 0);
  await proxyCloudWatch.putMetricDataExecutionTime(process.env.CIT_ENV, executionTime);
  const message = `Sent ${formattedEmails.length} emails for quantized time slot: ${currentTimeUTC} UTC in ${executionTime} seconds.`;
  const subject = `CRON: Reminder Job - Summary Report (${process.env.CIT_ENV})`;
  const htmlMessage = buildHTMLSummaryMessage(message);
  await proxySES.sendEmail(process.env.EMAIL_SUMMARY_REPORT, htmlMessage, subject);
  console.log(`*** CRON Reminder Job has finished, total run time: ${executionTime} seconds. ***` );
}

/**
 * Alert the authorities.
 */
const sendFatalErrorReport = async (err, currentTimeUTC) => {
  const subject = `CRON: Reminder Job - FATAL ERROR in (${process.env.CIT_ENV})`;
  console.log(`*** FATAL ERROR: ${err} stack: ${err.stack} ***`);
  const htmlMessage = buildHTMLFatalErrorMessage(err, currentTimeUTC);
  await proxySES.sendEmail(process.env.EMAIL_SUMMARY_REPORT, htmlMessage, subject);
}

/**
 * Rounds a datetime up or down to the nearest hour or half-hour mark.
 */
function quantize(dateTime) {
  let remainder = 0;
  if (dateTime.minute() >= 45 && dateTime.minute() <= 59) {
    remainder = 30 - (dateTime.minute() % 30);
  } else if (dateTime.minute() >= 1 && dateTime.minute() <= 15) {
    remainder = (dateTime.minute() % 30) * -1;
  } else if (dateTime.minute() >= 16 && dateTime.minute() <= 29) {
    remainder = 30 - (dateTime.minute() % 30);
  } else if (dateTime.minute() >= 31 && dateTime.minute() <= 44) {
    remainder = (dateTime.minute() % 30) * -1;
  }
  return moment(dateTime).add(remainder, "minutes").format("YYYY-MM-DD HH:mm:00");
}

/**
 * HTML TEMPLATES
 */
function buildHTMLMessage(htmlTable) {
  let compiled = _.template(`
   <html>
     <body>
       <h2>CalendarIt Reminder</h2>
       <hr>
       <%= htmlTable %>
       <p>&nbsp;</p>
       <img src="https://s3.amazonaws.com/cit-media/cit-logo.png" width="200"/>
     </body>
  </html>`);
  return htmlMessage = compiled({htmlTable});
}

function buildHTMLSummaryMessage(message) {
  let compiled = _.template(`
   <html>
     <body>
       <h2>Reminder Job Summary Report</h2>
       <hr>
       <table border=0>
         <tr>
           <td><%= message %></td>
         </tr>
       </table>
       <p>&nbsp;</p>
       <img src="https://s3.amazonaws.com/cit-media/cit-logo.png" width="200"/>
     </body>
  </html>`);
  return htmlMessage = compiled({message});
}

function buildHTMLFatalErrorMessage(error, currentTimeUTC) {
  let compiled = _.template(`
   <html>
     <body>
       <h2>Reminder Job Fatal Error</h2>
       <table border=0>
         <tr>
           <td>Error Message</td>
           <td><%= errorMessage %></td>
         </tr>
         <tr>
           <td>Stack Trace</td>
           <td><%= stackTrace %></td>
        </tr>
        <tr>
          <td>Time slot</td>
          <td><%= currentTimeUTC %></td>
        </tr>
       </table>
     </body>
  </html>`);
  return htmlMessage = compiled({ 
      errorMessage: error,
      stackTrace: error.stack,
      currentTimeUTC: currentTimeUTC
    });
}
