//
// Name: proxyCloudWatch.js
// Auth: Martin Burolla
// Date: 2/09/2019
// Desc: The one and only interface into AWS CloudWatch.
//

const AWS = require('aws-sdk');

AWS.config.update({region: 'us-east-1'});
const cloudwatch = new AWS.CloudWatch({apiVersion: '2015-10-07'});

module.exports.putMetricDataReminderEmails = async function(envName, numEmails) {
  const params = {
    MetricData: [{
      'MetricName': 'Reminder Emails',
      'Dimensions': [{'Name': 'Number Emails', 'Value': 'Sent'}],
      'Unit': 'Count',
      'Value': numEmails
    }],
    Namespace: envName 
  };
  return retval = await cloudwatch.putMetricData(params).promise();
};

module.exports.putMetricDataExecutionTime = async function(envName, numSeconds) {
  const params = {
    MetricData: [{
      'MetricName': 'Execution Time',
      'Dimensions': [{'Name': 'Time', 'Value': 'Seconds'}],
      'Unit': 'Count',
      'Value': numSeconds
    }],
    Namespace: envName 
  };
  return await cloudwatch.putMetricData(params).promise();
};
