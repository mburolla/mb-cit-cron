//
// Name: proxySES.js
// Auth: Martin Burolla
// Date: 1/31/2019
// Desc: Synchronously sends an email using the AWS SES service.
//

const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});

module.exports.sendEmail = async function(address, message, subject) {
  let retval = null;

  const params = {
    Destination: { 
      ToAddresses: [address]
    },
    Message: {
      Body: { 
        Html: {
          Charset: "UTF-8",
          Data: `<html><head></head><body>${message}</body></html>`
        },
        Text: {
          Charset: "UTF-8",
          Data: message
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: subject
      }
    },
    Source: 'noreply@calendarit.com', 
    ReplyToAddresses: ['noreply@calendarit.com'],
  };       

  try {
    retval = await new AWS.SES({apiVersion: '2010-12-01'}).sendEmail(params).promise();
  } catch(err) {
    console.log(err);
  }
  return retval;
};
