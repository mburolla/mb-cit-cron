// 
// Name: handler.js
// Auth: 
// Date: 
// Desc: The one and only handler for the CRON <NAME> job.
//

const dbpools = require('./dbpools');
const proxySES = require('./proxySES');
const proxyAuroraDB = require('./proxyAuroraDB');

const pool = dbpools.createRWPool().promise();

module.exports.<NAME> = async (event, context, callback) => { 
  // Validation & Security  /////////////////////////////////////
  console.log(`CRON Job is running in: ${process.env.CIT_ENV}`);

  // Business ///////////////////////////////////////////////////
  const r = await proxyAuroraDB.test(pool);
  await proxySES.sendEmail('<EMAIL>', JSON.stringify(r), '<SUBJECT>');
  
  // Presentation ////////////////////////////////////////////////
  console.log(r);
};
