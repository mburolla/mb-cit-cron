//
// Name: proxyAuroraDB.js
// Auth: Martin Burolla
// Date: 02/06/2019
// Desc: The one and only interface into AWS Aurora DB for the Reminder CRON job.
//

const CN_REMINDER_DEADLINES = 'CN_Reminder_Deadlines';

module.exports.test = async function (pool) {
  const sql = `call _InsertTest()`;
  const [[retval]] = await pool.query(sql);
  return retval;
};
